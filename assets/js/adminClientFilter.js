function applyFilter() {
	
	const rows = document.querySelectorAll('.client-table-row');
	
	const filterName  = document.getElementById('filter-name');
	const filterEmail = document.getElementById('filter-email');

	rows.forEach((row) => {
		
		if ( determineRowState(row, {
			name : filterName.value.trim().toLowerCase(),
			email : filterEmail.value.trim().toLowerCase(),
		}) ) {
			
			row.style.display = "table-row";
			
		} else {
			row.style.display = "none"
		}
	});
}

function determineRowState(row, values) {
	const name = row.getElementsByClassName('client-name')[0].innerText.trim().toLowerCase();
	const email = row.getElementsByClassName('client-email')[0].innerText.trim().toLowerCase();
	
	if ( !name.includes(values.name) ) return false;
	if ( !email.includes(values.email) ) return false;
	
	return true;
}