function applyFilter() {
	
	const rows = document.querySelectorAll('.client-table-row');
	
	const filterDate   = document.getElementById('filter-date');
	const filterTitle  = document.getElementById('filter-title');
	const filterStatus = document.getElementById('filter-status');
	const filterResult = document.getElementById('filter-result');

	rows.forEach((row) => {
		
		if ( determineRowState(row, {
			date   : filterDate.value.trim().toLowerCase(),
            title  : filterTitle.value.trim().toLowerCase(),
			status : filterStatus.value.trim().toLowerCase(),
			result : filterResult.value.trim().toLowerCase(),
		}) ) {
			
			row.style.display = "table-row";
			
		} else {
			row.style.display = "none"
		}
	});
}

function determineRowState(row, values) {
	const date   = row.getElementsByClassName('file-created-at')[0].innerText.trim().toLowerCase();
	const title  = row.getElementsByClassName('file-title')[0].innerText.trim().toLowerCase();
	const status = row.getElementsByClassName('file-status')[0].innerText.trim().toLowerCase();
	const result = row.getElementsByClassName('file-result')[0].innerText.trim().toLowerCase();
	
	if ( !date.includes(values.date) ) return false;
	if ( !title.includes(values.title) ) return false;
	if ( !status.includes(values.status) ) return false;
	if ( !result.includes(values.result) ) return false;
	
	return true;
}

function perPagePickerChangeHandler() {
	const perPageValue = document.querySelector('#per-page-picker option:checked').value;
	window.location = BASE + "admin/files/" + perPageValue;
}