function applyFilter() {
	
	const rows = document.querySelectorAll('.client-table-row');
	
	const filterDate   = document.getElementById('filter-date');
	const filterClient = document.getElementById('filter-client');

	rows.forEach((row) => {
		
		if ( determineRowState(row, {
			date   : filterDate.value.trim().toLowerCase(),
            client : filterClient.value.trim().toLowerCase(),
		}) ) {
			
			row.style.display = "table-row";
			
		} else {
			row.style.display = "none"
		}
	});
}

function determineRowState(row, values) {
	const date   = row.getElementsByClassName('folder-created-at')[0].innerText.trim().toLowerCase();
	const client = row.getElementsByClassName('folder-client-name')[0].innerText.trim().toLowerCase();
	
	if ( !date.includes(values.date) ) return false;
	if ( !client.includes(values.client) ) return false;
	
	return true;
}