function applyFilter() {
	
	const rows = document.querySelectorAll('.client-table-row');
	
	const filterDate   = document.getElementById('filter-date');
	const filterDocument = document.getElementById('filter-document');
	const filterStatus = document.getElementById('filter-status');

	rows.forEach((row) => {
		
		if ( determineRowState(row, {
			date     : filterDate.value.trim().toLowerCase(),
            document : filterDocument.value.trim().toLowerCase(),
            status   : filterStatus.value.trim().toLowerCase(),
		}) ) {
			
			row.style.display = "table-row";
			
		} else {
			row.style.display = "none"
		}
	});
}

function determineRowState(row, values) {
	const date     = row.getElementsByClassName('folder-created-at')[0].innerText.trim().toLowerCase();
	const document = row.getElementsByClassName('folder-document-title')[0].innerText.trim().toLowerCase();
	const status   = row.getElementsByClassName('folder-status')[0].innerText.trim().toLowerCase();
	
	if ( !date.includes(values.date) ) return false;
	if ( !document.includes(values.document) ) return false;
	if ( !status.includes(values.status) ) return false;
	
	return true;
}