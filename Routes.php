<?php
    return [
        #LOGIN + REGISTER USER
        App\Core\Route::get('|^user/register/?$|',                      'Main',                      'getRegister'),
        App\Core\Route::post('|^user/register/?$|',                     'Main',                      'postRegister'),
        App\Core\Route::get('|^user/login/?$|',                         'Main',                      'getLogin'),
        App\Core\Route::post('|^user/login/?$|',                        'Main',                      'postLogin'),
        App\Core\Route::get('|^user/logout/?$|',                        'Main',                      'getLogout'),
        #LOGIN + REGISTER ADMIN
        App\Core\Route::get('|^admin/register/?$|',                     'Main',                      'getAdminRegister'),
        App\Core\Route::post('|^admin/register/?$|',                    'Main',                      'postRegisterAdmin'),
        App\Core\Route::get('|^admin/login/?$|',                        'Main',                      'getLoginAdmin'),
        App\Core\Route::post('|^admin/login/?$|',                       'Main',                      'postLoginAdmin'),
        App\Core\Route::get('|^admin/logout/?$|',                       'Main',                      'getAdminLogout'),
        # --------

        # USER ROLE ROUTES:
        App\Core\Route::get('|^user/profile/?$|',                       'UserDashboard',             'index'),
        # USER FOLDER LIST
        App\Core\Route::get('|^user/folders/?$|',                       'UserFolderManagement',      'folders'),
        # USER ENTERING FOLDER
        App\Core\Route::get('|^user/folder/([0-9]+)/?$|',               'UserFolderManagement',      'folder'),
        # USER VIEW REPORT
        App\Core\Route::get('|^user/report/([0-9]+)/?$|',               'UserFolderManagement',      'report'),
        # USER DOWNLOAD FOLDER
        App\Core\Route::get('|^user/folder/([0-9]+)/archive/?$|',       'UserFolderManagement',      'downloadArchive'),
        # USER FILE LIST
        App\Core\Route::get('@^user/files(?:/([0-9]+)(?:/([0-9]+))?(?:/(created_at|title|status)-(ASC|DESC))?)?/?$@',  'UserFileManagement',        'files'),
        # USER FILE UPLOAD
        App\Core\Route::get('|^user/upload-files/?$|',                  'UserFileManagement',        'fileUpload'),
        # USER POST FILE UPLOAD
        App\Core\Route::post('|^user/uploaded-files/?$|',               'UserFileManagement',        'postFileUpload'),
        # USER GET FILE DONWLOAD
        App\Core\Route::get('|^user/download/([0-9]+)/?$|',             'UserFileManagement',        'download'),
        # --------

        # ADMIN ROLE ROUTES:
        App\Core\Route::get('|^admin/profile/?$|',                      'AdminDashboard',            'index'),
        # ADMIN LIST USERRS:
        App\Core\Route::get('|^admin/clients/?$|',                      'AdminDashboard',            'clients'),
        # ADMIN FILTER USERS:
        App\Core\Route::post('|^admin/filter-users/?$|',                'AdminDashboard',            'filterUsers'),
        # ADMIN FOLDER LIST
        App\Core\Route::get('|^admin/folders/?$|',                      'AdminFolderManagement',     'folders'),
        # ADMIN FOLDER LIST
        App\Core\Route::get('|^admin/folder/([0-9]+)/?$|',              'AdminFolderManagement',     'folder'),
        # ADMIN FILE LIST
        App\Core\Route::get('@^admin/files(?:/([0-9]+)(?:/([0-9]+))?(?:/(created_at|title|status)-(ASC|DESC))?)?/?$@',   'AdminFileManagement',          'files'),
        # ADMIN FILE UPLOAD
        App\Core\Route::get('|^admin/upload-files/?$|',                 'AdminFileManagement',       'fileUpload'),
        # ADMIN GET FILE DONWLOAD
        App\Core\Route::get('|^admin/download/([0-9]+)/?$|',            'AdminFileManagement',       'download'),
        # ADMIN POST FILE REPORT STATUS CHANGE
        App\Core\Route::post('|^admin/folder/([0-9]+)/file/([0-9]+)/changeStatus/?$|', 'AdminFileManagement', 'fileStatusChange'),
        # --------

        # FALLBACK ROUTE
        App\Core\Route::any('|^.*$|',                                   'Main',                      'home')
    ];