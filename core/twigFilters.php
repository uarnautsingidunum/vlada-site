<?php
    function intervalDate($date) {
        $now = time();
        $time = strtotime($date);

        if ($now - $time < 60) return "A few moments ago";
        if ($now - $time < 5*60) return "Less than 5 minutes ago";
        if ($now - $time < 15*60) return "Less than 15 minutes ago";
        if ($now - $time < 30*60) return "Less than 30 minutes ago";
        if ($now - $time < 60*60) return "Less than 1h ago";
        if ($now - $time < 2*60*60) return "Less than 2h ago";
        if ($now - $time < 12*60*60) return "Less than 12h ago";
        if ($now - $time < 24*60*60) return "Less than a day ago";

        return $date;
    }
