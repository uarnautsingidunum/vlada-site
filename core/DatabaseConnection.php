<?php
    namespace App\Core;

    class DatabaseConnection {
        private $databaseConfiguration;
        private $databaseConnection = NULL;

        public function __construct(DatabaseConfiguration $config) {
            $this->databaseConfiguration = $config;
        }

        public function getDatabaseConnection(): \PDO {
            if ($this->databaseConnection === NULL) {
                $this->databaseConnection = new \PDO(
                    $this->databaseConfiguration->getSourceString(),
                    $this->databaseConfiguration->getUsername(),
                    $this->databaseConfiguration->getPassword()
                );
            }

            return $this->databaseConnection;
        }

        final public function beginTransaction() {
            $prep = $this->databaseConnection->prepare('START TRANSACTION;');
            return $prep->execute();
        }

        final public function commitTransaction() {
            $prep = $this->databaseConnection->prepare('COMMIT;');
            return $prep->execute();
        }

        final public function rollbackTransaction() {
            $prep = $this->databaseConnection->prepare('ROLLBACK;');
            return $prep->execute();
        }
    }
