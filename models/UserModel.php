<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;

    class UserModel extends Model {
        protected function getFields(): array {
            return [
                'user_id'           => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'        => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                
                'name'              => new Field((new \App\Validators\StringValidator)->setMaxLength(64) ),
                'surname'           => new Field((new \App\Validators\StringValidator)->setMaxLength(64) ),
                'id_number'         => new Field((new NumberValidator())->setIntegerLength(9), true),
                'email'             => new Field((new \App\Validators\StringValidator)->setMaxLength(128) ),
                'username'          => new Field((new \App\Validators\StringValidator)->setMaxLength(32) ),
                'password_hash'     => new Field((new \App\Validators\StringValidator(0, 128)) ),

                'is_active'         => new Field((new \App\Validators\BitValidator()))
            ];
        }

        public function getByUsername(string $username) {
            return $this->getByFieldName('username', $username);
        }

        public function getAllByFilters(string $name, string $email) {
            $sql = 'SELECT * FROM `user` WHERE `name` LIKE ? AND `email` LIKE ?;';

            $filterName  = '%' . $name . '%';
            $filterEmail = '%' . $email . '%';

            $prep = $this->getConnection()->prepare($sql);

            if ( !$prep ) {
                return [];
            }

            $res = $prep->execute([$filterName, $filterEmail]);

            if ( !$res ) {
                return [];
            }

            return $prep->fetchAll(\PDO::FETCH_OBJ);
        }
    }
