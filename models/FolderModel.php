<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class FolderModel extends Model {
        protected function getFields(): array {
            return [
                'folder_id'    => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),

                'user_id'    => new Field((new NumberValidator())->setIntegerLength(11), true),
                'name'      => new Field((new \App\Validators\StringValidator)->setMaxLength(255) )
            ];
        }

        public function getAllByName(string $name) {
            return $this->getByFieldName('name', $name);
        }

        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
        }

        public function getFiles(int $folderId): array {
            $sql = 'SELECT
                        `folder`.`name`,
                        `user`.`name`,
                        `file`.`file_id`,
                        `file`.`title`,
                        `file`.`created_at`,
                        `file`.`path`,
                        `file`.`status`,
                        `report`.`report_type`
                    FROM `folder` 
                    INNER JOIN `file` ON `folder`.`folder_id` = `file`.`folder_id` 
                    INNER JOIN `user` ON `file`.`user_id` = `user`.`user_id`
                    LEFT JOIN `report` ON `report`.`file_id` = `file`.`file_id` 
                    WHERE
                        `folder`.`folder_id` = ?
                    ORDER BY
                        `file`.`file_id`;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$folderId]);
            $list = [];

            if ($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $list;
        }

        public function getFolderData(int $folderId): \stdClass {
            $sql = 'SELECT * FROM folder WHERE folder_id = ?';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$folderId]);

            $item = null;

            if ($res){
                $item = $prep->fetch(\PDO::FETCH_OBJ);
            }

            return $item;
        }
    }