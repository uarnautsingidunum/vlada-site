<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\BitValidator;
    use App\Validators\DateTimeValidator;

    class ReportModel extends Model {
        protected function getFields(): array {
            return [
                'file_id'     => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at'  => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                
                'file_id'     => new Field((new NumberValidator())->setIntegerLength(11), true),
                'user_id'     => new Field((new NumberValidator())->setIntegerLength(11), true),
                'admin_id'    => new Field((new NumberValidator())->setIntegerLength(11), true),
                
                'report_type' => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'comment'     => new Field((new \App\Validators\StringValidator)->setMaxLength(65000) )
            ];
        }

        public function getAllByFileId(int $fileId): array {
            return $this->getAllByFieldName('file_id', $fileId);
        }
        
        public function getAllByName(string $name) {
            return $this->getByFieldName('name', $name);
        }

        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
        }

        public function getClientFolderReport(int $folderId): array {
            $sql = 'SELECT `report`.`created_at`, `report`.`file_id`, `report`.`admin_id`, `report`.`user_id`, `report`.`report_type`, `report`.`comment`, `file`.`title`, `folder`.`name`, `user`.`username`
                    FROM `report` 
                    INNER JOIN `file` ON `report`.`file_id` = `file`.`file_id` 
                    INNER JOIN `folder` ON `file`.`folder_id` = `folder`.`folder_id`
                    INNER JOIN `user` ON `folder`.`user_id` = `user`.`user_id`
                    WHERE `folder`.`folder_id` = ?; ORDER BY `report`.`created_at` ASC;';
            $prep = $this->getConnection()->prepare($sql);
            $res = $prep->execute([$folderId]);
            $list = [];

            if($res){
                $list = $prep->fetchAll(\PDO::FETCH_OBJ);
            }

            return $list;
        }
    }