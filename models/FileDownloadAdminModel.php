<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\StringValidator;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class FileDownloadAdminModel extends Model {
        protected function getFields(): array {
            return [
                'file_download_id' => new Field((new NumberValidator())->setIntegerLength(20), false),
                'created_at'       => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                'admin_id'         => new Field((new NumberValidator())->setIntegerLength(11), true),
                'file_id'          => new Field((new NumberValidator())->setIntegerLength(11), true),
                'user_agent'       => new Field((new StringValidator)->setMaxLength(255) ),
                'ip_address'       => new Field((new StringValidator)->setMaxLength(64) ),
            ];
        }

        public function getAllByFileId(int $fileId): array {
            return $this->getAllByFieldName('file_id', $fileId);
        }

        public function getAllByUserId(int $adminId): array {
            return $this->getAllByFieldName('admin_id', $adminId);
        }
    }
