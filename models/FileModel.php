<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;

    class FileModel extends Model {
        protected function getFields(): array {
            return [
                'file_id'    => new Field((new \App\Validators\NumberValidator())->setIntegerLength(11), false),
                'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(), false ),
                'user_id'    => new Field((new NumberValidator())->setIntegerLength(11), true),
                'title'      => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'path'       => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'status'     => new Field((new \App\Validators\StringValidator)->setMaxLength(255) ),
                'folder_id'  => new Field((new NumberValidator())->setIntegerLength(11), true),
            ];
        }

        public function getAllByFileId(int $fileId): array {
            return $this->getAllByFieldName('file_id', $fileId);
        }
        
        public function getAllByName(string $name) {
            return $this->getByFieldName('name', $name);
        }

        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
        }

        public function getAllByUserIdPaged(int $userId, int $perPage, int $page): array {
            return $this->getAllByFieldNamePaged('user_id', $userId, $perPage, $page);
        }

        public function getAllByUserIdPagedOrdered(int $userId, int $perPage, int $page, $field, $direction): array {
            return $this->getAllByFieldNamePagedOrdered('user_id', $userId, $perPage, $page, $field, $direction);
        }

        public function getAllByAdminPagedOrdered(int $perPage, int $page, $field, $direction): array {
            return $this->getAllAdminPagedOrdered($perPage, $page, $field, $direction);
        }
    }