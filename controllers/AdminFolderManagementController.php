<?php
    namespace App\Controllers;

    class AdminFolderManagementController extends \App\Core\Role\AdminRoleController {

        public function folders() {

            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());
            $folders = $folderModel->getAll();
            $this->set('folders', $folders);
        }

        public function folder($folderId) {
            $folderId = intval($folderId);
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());

            #folder
            $folder = $folderModel->getById($folderId);
            $this->set('folder', $folder);

            #files
            $filesInFolder = $folderModel->getFiles($folderId);
            $this->set('filesInFolder', $filesInFolder);
        }
    }
