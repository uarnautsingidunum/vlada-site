<?php
    namespace App\Controllers;

    class AdminDashboardController extends \App\Core\Role\AdminRoleController {
        public function index() {
            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $files = $fileModel->getAll();
            $this->set('files', $files);
        }

        public function clients() {
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $clients = $userModel->getAll();
            $this->set('clients', $clients);
        }

        public function filterUsers() {

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $name  = filter_input(INPUT_POST, 'global-name-filter',  FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'global-email-filter', FILTER_SANITIZE_STRING);

            $filterName  = trim($name);
            $filterName  = preg_replace('/ +/', ' ', $filterName);

            $filterEmail = trim($email);
            $filterEmail = preg_replace('/ +/', ' ', $filterEmail);

            $clients = $userModel->getAllByFilters($name, $email);

            $this->set('clients', $clients);
        }
    }
