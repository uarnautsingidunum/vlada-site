<?php
    namespace App\Controllers;

    class UserDashboardController extends \App\Core\Role\UserRoleController {
        public function index() {
            $userId = $this->getSession()->get('user_id');

            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $files = $fileModel->getAllByUserId($userId);
            $this->set('files', $files);
        }
    }
