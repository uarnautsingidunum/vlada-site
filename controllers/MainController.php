<?php
    namespace App\Controllers;

    class MainController extends \App\Core\Controller{

        public function home(){
            
        }

        #dodat novi kod
        public function getRegister() {
            
        }

        public function postRegister() {
            $name       = \filter_input(INPUT_POST, 'reg_name', FILTER_SANITIZE_STRING);
            $surname    = \filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
            $id_number  = \filter_input(INPUT_POST, 'reg_id_number', FILTER_SANITIZE_NUMBER_INT);
            $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
            $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
            $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

            if ($password1 !== $password2) {
                $this->set('message', 'An error occurred: You did not enter the same password twice.');
                return;
            }

            $validanPassword = (new \App\Validators\StringValidator())
                ->setMinLength(7)
                ->setMaxLength(120)
                ->isValid($password1);

            if ( !$validanPassword) {
                $this->set('message', 'An error occurred: The password format does not match.');
                return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $user = $userModel->getByFieldName('email', $email);
            if ($user) {
                $this->set('message', 'An error has occurred: There is already a user with this email address.');
                return;
            }

            $user = $userModel->getByFieldName('username', $username);
            if ($user) {
                $this->set('message', 'An error occurred: There is already a user with this username.');
                return;
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

            $userId = $userModel->add([
                'name'          => $name,
                'surname'       => $surname,
                'id_number'     => $id_number,
                'email'         => $email,
                'username'      => $username,
                'password_hash' => $passwordHash,     
            ]);

            if (!$userId) {
                $this->set('message', 'An error occurred: Account registration failed.');
                return;
            }

            $this->set('message', 'A new account has been created. You can now sign up.');
        }

    #USER LOGIN
    public function getLogin() {
        
    }

    public function postLogin() {
        $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password);

        if ( !$validanPassword) {
            $this->set('message', 'An error occurred: The password format does not match.');
            return;
        }

        $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

        $user = $userModel->getByFieldName('username', $username);

        if (!$user) {
            $this->set('message', 'An error occurred: There is no user with this username.');
            return;
        }

        if (!password_verify($password, $user->password_hash)) {
            sleep(1); #usporava brute force attack, tako sto ceka 1sec na msg za neispravnu pswd
            $this->set('message', 'An error occurred: The password is incorrect.');
            return;
        }

        $userLoginModel = new \App\Models\UserLoginModel($this->getDatabaseConnection());
        $userLoginModel->add([
            'user_id' => $user->user_id,
            'ip_address' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING),
        ]);

        $this->getSession()->put('user_id', $user->user_id);
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE . 'user/profile');
    }
    #USER LOGIN.
    
    public function getLogout() {
        $this->getSession()->remove('user_id');
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE);
    }
    
	#ADMIN REGISTER
	public function getAdminRegister() {
			
	}

    public function postRegisterAdmin() {
        $name       = \filter_input(INPUT_POST, 'reg_name', FILTER_SANITIZE_STRING);
        $surname    = \filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
        $email      = \filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
        $username   = \filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
        $password1  = \filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
        $password2  = \filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

        if ($password1 !== $password2) {
            $this->set('message', 'An error occurred: You did not enter the same password twice.');
            return;
        }

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password1);
        
        if ( !$validanPassword) {
            $this->set('message', 'An error occurred: The password format does not match.');
            return;
        }
        
        $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());

        $admin = $adminModel->getByFieldName('email', $email);
        if ($admin) {
            $this->set('message', 'An error has occurred: There is already a user with this email address.');
            return;
        }

        $admin = $adminModel->getByFieldName('username', $username);
        if ($admin) {
            $this->set('message', 'An error occurred: There is already a user with this username.');
            return;
        }

        $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);

        $adminId = $adminModel->add([
            'name'          => $name,
            'surname'       => $surname,
            'email'         => $email,
            'username'      => $username,
            'password_hash' => $passwordHash,     
        ]);

        if (!$adminId) {
            $this->set('message', 'An error occurred: Account registration failed.');
            return;
        }

        $this->set('message', 'A new account has been created. You can now sign up.');
    }
	#ADMIN REGISTER.
	
	#ADMIN LOGIN
	public function getLoginAdmin() {

        }

    public function postLoginAdmin() {
        $username = \filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = \filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

        $validanPassword = (new \App\Validators\StringValidator())
            ->setMinLength(7)
            ->setMaxLength(120)
            ->isValid($password);

        if ( !$validanPassword) {
            $this->set('message', 'An error occurred: The password format does not match.');
            return;
        }

        $adminModel = new \App\Models\AdminModel($this->getDatabaseConnection());

        $admin = $adminModel->getByFieldName('username', $username);
        if (!$admin) {
            $this->set('message', 'An error occurred: There is no ADMIN with that username.');
            return;
        }

        if (!password_verify($password, $admin->password_hash)) {
            sleep(1); #usporava brute force attack, tako sto ceka 1sec na msg za neispravnu pswd
            $this->set('message', 'An error occurred: The password is incorrect.');
            return;
        }

        $adminLoginModel = new \App\Models\AdminLoginModel($this->getDatabaseConnection());
        $adminLoginModel->add([
            'admin_id' => $admin->admin_id,
            'ip_address' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING),
        ]);

        $this->getSession()->put('admin_id', $admin->admin_id);
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE . 'admin/profile');
    }
    #ADMIN LOGIN.

    public function getAdminLogout() {
        $this->getSession()->remove('admin_id');
        $this->getSession()->save();
        $this->redirect(\Configuration::BASE);
    }

    }