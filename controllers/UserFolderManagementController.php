<?php
    namespace App\Controllers;

    use \ZipStream\Option\Archive;

    class UserFolderManagementController extends \App\Core\Role\UserRoleController {

        public function folders() {
            $userId = $this->getSession()->get('user_id');

            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());
            $folders = $folderModel->getAllByUserId($userId);
            $this->set('folders', $folders);
        }

        public function folder($folderId) {
            $folderId = intval($folderId);
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());

            #folder
            $folder = $folderModel->getById($folderId);

            if ($folder->user_id != $this->getSession()->get('user_id')) {
                return $this->set('message', 'This is not your folder.');
            }

            $this->set('folder', $folder);

            #files
            $filesInFolder = $folderModel->getFiles($folderId);
            $this->set('filesInFolder', $filesInFolder);
        }

        public function report($folderId) {
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());

            #for folder info
            $folder = $folderModel->getFolderData($folderId);
            if ($folder->user_id != $this->getSession()->get('user_id')) {
                return $this->set('message', 'This is not your folder.');
            }

            $this->set('folder', $folder);

            $reportModel = new \App\Models\ReportModel($this->getDatabaseConnection());
            #for everything else
            $reports = $reportModel->getClientFolderReport($folderId);
            $this->set('reports', $reports);
        }

        public function downloadArchive($folderId) {
            $folderId = intval($folderId);
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());

            $folder = $folderModel->getById($folderId);
            if ($folder->user_id != $this->getSession()->get('user_id')) {
                return $this->set('message', 'This is not your folder.');
            }
            
            $filesInFolder = $folderModel->getFiles($folderId);

            $name = '"' . date('Y-m-d') . ' ' . $folder->name . '.zip"';

            $options = new Archive();
            $options->setSendHttpHeaders(true);
            $options->setEnableZip64(true);
            $options->setFlushOutput(true);
            $options->setContentDisposition(urlencode($name));

            $zip = new \ZipStream\ZipStream($name, $options);

            foreach ($filesInFolder as $file) {
                $zip->addFileFromPath($file->title, $file->path);
            }

            $zip->finish();
            exit;
        }
    }
