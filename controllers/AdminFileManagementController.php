<?php
    namespace App\Controllers;

    class AdminFileManagementController extends \App\Core\Role\AdminRoleController {
        public function index() {
            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $files = $fileModel->getAll();
            $this->set('files', $files);
        }

        public function files(
            string $perPage="20",
            string $page="0",
            string $orderBy="created_at",
            string $orderDirection="DESC"
        ) {
            if (!$perPage) $perPage = 20;
            if (!$page) $page = 0;
            if (!$orderBy) $orderBy = "created_at";
            if (!$orderDirection) $orderDirection = "DESC";

            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $files = $fileModel->getAllByAdminPagedOrdered(intval($perPage), intval($page), $orderBy, $orderDirection);
            #$files     = $fileModel->getAll();
            $this->set('files', $files);

            $this->set('perPage', $perPage);
            $this->set('page', $page);
            $this->set('orderBy', $orderBy);
            $this->set('orderDirection', $orderDirection);
            $this->set('inverseOrderDirection', $orderDirection == "ASC" ? "DESC" : "ASC");

            $this->set('perPageOptions', [ 10, 20, 50, 100, 100_000_000_000 ]);
        }

        public function download(int $fileId) {
            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());

            $file = $fileModel->getById($fileId);

            if ($file === null) {
                ob_clean();
                header('Content-type: text/html', true, 404);
                die('Resource not found!');
                exit;
            }

            $currentUserId = $this->getSession()->get('admin_id');

            /*if ($file->user_id != $currentUserId) {
                ob_clean();
                header('Content-type: text/html', true, 403);
                die('Access denied to this resource!');
                exit;
            }*/

            $fileDownloadAdminModel = new \App\Models\FileDownloadAdminModel($this->getDatabaseConnection());
            $fileDownloadAdminModel->add([
                "file_id"    => $file->file_id,
                "admin_id"   => $currentUserId,
                "user_agent" => filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING),
                "ip_address" => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING), 
            ]);

            ob_clean();
            header('Content-type: application/force-download', true);
            header('Content-transfer-encoding: binary', true);
            header('Content-disposition: attachment; filename="' . $file->title . '.pdf"', true);
            readfile($file->path);

            exit;
        }

        public function fileStatusChange($folderId, $fileId) {
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());
            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $reportModel = new \App\Models\ReportModel($this->getDatabaseConnection());

            $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);

            if (!in_array($status, ['Undetermined', 'Edited', 'Clean'])) {
                $this->set('message', 'Invalid status.');
                return;
            }

            $folder = $folderModel->getById($folderId);
            if (!$folder) {
                $this->set('message', 'Folder does not exist.');
                return;
            }

            $file = $fileModel->getById($fileId);
            if (!$file) {
                $this->set('message', 'File does not exist.');
                return;
            }

            if ($file->folder_id != $folderId) {
                $this->set('message', 'This file does not belong to the selected folder.');
                return;
            }

            $existingReports = $reportModel->getAllByFileId($fileId);
            if (count($existingReports) > 0) {
                $existingReport = $existingReports[0];
                $reportModel->editById($existingReport->report_id, [
                    'report_type' => $status
                ]);
            } else {
                $reportModel->add([
                    'file_id'     => $fileId,
                    'admin_id'    => $this->getSession()->get('admin_id'),
                    'user_id'     => $folder->user_id,
                    'report_type' => $status,
                ]);
            }

            $fileModel->editById($fileId, [ 'status' => 'Ready' ]);

            $this->redirect(\Configuration::BASE . 'admin/folder/' . $folderId . '/#file-' . $fileId);
        }
    }
