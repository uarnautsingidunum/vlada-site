<?php
    namespace App\Controllers;

    class UserFileManagementController extends \App\Core\Role\UserRoleController {

        public function files(
            string $perPage="20",
            string $page="0",
            string $orderBy="created_at",
            string $orderDirection="DESC"
        ) {
            if (!$perPage) $perPage = 20;
            if (!$page) $page = 0;
            if (!$orderBy) $orderBy = "created_at";
            if (!$orderDirection) $orderDirection = "DESC";

            $userId = $this->getSession()->get('user_id');

            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());
            $files = $fileModel->getAllByUserIdPagedOrdered($userId, intval($perPage), intval($page), $orderBy, $orderDirection);

            $this->set('files', $files);
            $this->set('perPage', $perPage);
            $this->set('page', $page);
            $this->set('orderBy', $orderBy);
            $this->set('orderDirection', $orderDirection);
            $this->set('inverseOrderDirection', $orderDirection == "ASC" ? "DESC" : "ASC");

            $this->set('perPageOptions', [ 10, 20, 50, 100, 100_000_000_000 ]);
        }

        public function fileUpload() {
            $userId = $this->getSession()->get('user_id');
            $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());
            $folders = $folderModel->getAllByUserId($userId);
            // sort
            $this->set('folders', $folders);
        }

        private function normalize($string) {
            return preg_replace('/[^A-Za-z0-9\-\_\.]/', '', $string);
        }

        public function download(int $fileId) {
            $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());

            $file = $fileModel->getById($fileId);

            if ($file === null) {
                ob_clean();
                header('Content-type: text/html', true, 404);
                die('Resource not found!');
                exit;
            }

            $currentUserId = $this->getSession()->get('user_id');

            if ($file->user_id != $currentUserId) {
                ob_clean();
                header('Content-type: text/html', true, 403);
                die('Access denied to this resource!');
                exit;
            }

            $fileDownloadModel = new \App\Models\FileDownloadModel($this->getDatabaseConnection());
            $fileDownloadModel->add([
                "file_id"    => $file->file_id,
                "user_id"    => $currentUserId,
                "user_agent" => filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_STRING),
                "ip_address" => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING), 
            ]);

            ob_clean();
            header('Content-type: application/force-download', true);
            header('Content-transfer-encoding: binary', true);
            header('Content-disposition: attachment; filename="' . $file->title . '.pdf"', true);
            readfile($file->path);

            exit;
        }

        public function postFileUpload() {
            if (isset($_POST["submit"])) {
                $folderModel = new \App\Models\FolderModel($this->getDatabaseConnection());
                $fileModel = new \App\Models\FileModel($this->getDatabaseConnection());

                $userId = $this->getSession()->get('user_id');

                $selectedFolderId = filter_input(INPUT_POST, 'select-existing-folder');
                if ($selectedFolderId == '') {
                    $newFolderName = filter_input(INPUT_POST, 'input-new-folder-name', FILTER_SANITIZE_STRING);
                    $newFolderId = $folderModel->add([
                        'user_id' => $userId,
                        'name'    => $newFolderName
                    ]);

                    if (!$newFolderId) {
                        $this->set('message', 'Sorry, could not create a new folder at this time.');
                        return;
                    }

                    $selectedFolderId = $newFolderId;
                } else {
                    $folder = $folderModel->getById(intval($selectedFolderId));

                    if ($folder->user_id != $userId) {
                        $this->set('message', 'Sorry, this is not your folder.');
                        return;
                    }
                }

                $fileGroupCount = filter_input(INPUT_POST, 'file_groups', FILTER_SANITIZE_NUMBER_INT);
                if ($fileGroupCount == 1) {
                    $this->set('message', 'Sorry, there is nothing to upload.');
                    return;
                }

                $target_dir = './uploads/' . $userId . '/' . $selectedFolderId . '/';

                @mkdir($target_dir, 0755, true);

                $this->getDatabaseConnection()->beginTransaction();

                $tempPaths = [];

                for ($i=1; $i<$fileGroupCount; $i++) {
                    $fileGroup = $_FILES['files_' . $i];
                    $fileCount = count($fileGroup['name']);
                    for ($fId=0; $fId<$fileCount; $fId++) {
                        $baseName = basename($fileGroup['name'][$fId]);
                        $target_file = $target_dir . date('YmdHis') . '-' . rand(1000000, 9999999) . '-' . $this->normalize($baseName);
                        $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

                        $tempPaths[] = $target_file;

                        if (file_exists($target_file)) {
                            $this->set('message', 'Sorry, file ' . htmlspecialchars($baseName) . ' already exists. No files have been uploaded due to this error.');
                            $this->getDatabaseConnection()->rollbackTransaction();
                            $this->deleteFiles($tempPaths);
                            return;
                        }

                        if ( $fileGroup['size'][$fId] > 1024 * 1024 * 10 ) {
                            $this->set('message', 'Sorry, file ' . htmlspecialchars($baseName) . ' is too large. No files have been uploaded due to this error.');
                            $this->getDatabaseConnection()->rollbackTransaction();
                            $this->deleteFiles($tempPaths);
                            return;
                        }

                        if ($fileType != "pdf") {
                            $this->set('message', 'Sorry, only PDF files are allowed. See to file ' . htmlspecialchars($baseName) . '. No files have been uploaded due to this error.');
                            $this->getDatabaseConnection()->rollbackTransaction();
                            $this->deleteFiles($tempPaths);
                            return;
                        }

                        if (move_uploaded_file($fileGroup['tmp_name'][$fId], $target_file)) {
                            $fileId = $fileModel->add([
                                'user_id'   => $userId,
                                'title'     => $baseName,
                                'path'      => $target_file,
                                'folder_id' => $selectedFolderId,
                            ]);
    
                            if (!$fileId) {
                                $this->set('message', 'There was an error: Sorry, your file ' . htmlspecialchars($baseName) . ' was not uploaded. No files have been uploaded due to this error.');
                                $this->getDatabaseConnection()->rollbackTransaction();
                                $this->deleteFiles($tempPaths);
                                return;
                            }
                        }
                    }
                }

                $this->set('message', "All files have been uploaded.");
                $this->getDatabaseConnection()->commitTransaction();
            }
        }

        function deleteFiles(array $tempPaths) {
            foreach ($tempPaths as $tempPath) {
                @unlink($tempPath);
            }
        }
    }
