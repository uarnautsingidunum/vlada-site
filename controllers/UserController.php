<?php
    namespace App\Controllers;

    class UserController extends \App\Core\Controller{

        public function show($id){
            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getById($id);

            if(!$user){
                header(\Configuration::BASE);
                exit;
            }

            $this->set('user', $user);
    }
}